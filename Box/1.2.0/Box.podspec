Pod::Spec.new do |s|
  s.name         = "Box"
  s.version      = "1.2.0"
  s.summary      = "Collection of must-have functional Swift tools."
  s.homepage     = "https://github.com/robrix/Box"
  s.license      = "MIT"
  s.author             = { "Gordon Fontenot" => "gordon@thoughtbot.com" }

  s.ios.deployment_target = "8.0"
  s.osx.deployment_target = "10.10"
  s.source       = { :git => "https://github.com/robrix/Box", :tag => "1.2.0" }
  s.source_files  = "Box/*.swift"
end
